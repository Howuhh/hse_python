import os
import numpy as np

from matrix import Matrix

def main():
    np.random.seed(42)

    arr1 = np.random.randint(0, 10, (10, 10))
    arr2 = np.random.randint(0, 10, (10, 10))

    mat1, mat2 = Matrix(arr1), Matrix(arr2)

    with open("artifacts/easy/matrix+.txt", "w") as f:
        f.write(str(mat1 + mat2))

    with open("artifacts/easy/matrix*.txt", "w") as f:
        f.write(str(mat1 * mat2))

    with open("artifacts/easy/matrix@.txt", "w") as f:
        f.write(str(mat1 @ mat2))


if __name__ == "__main__":
    main()