
import numpy as np


class Matrix:
    def __init__(self, values):
        self.values = values

    @property
    def shape(self):
        return self.values.shape

    def __check_dims(self, other, op="add"):
        if op in ("add", "mul"):
            if not all(i == j for (i, j) in zip(self.shape, other.shape)):
                raise RuntimeError("Matrices should have same shapes.")
        elif op == "matmul":
            if self.shape[1] != other.shape[0]:
                raise RuntimeError("Matrices should have same 1 and 0 dim for multiplication") 
        else:
            raise RuntimeError("Unknown operation")

    def __add__(self, other):
        self.__check_dims(other, "add")

        result = np.zeros_like(self.values)
        for i in range(self.shape[0]):
            for j in range(self.shape[1]):
                result[i, j] = self.values[i, j] + other.values[i, j]

        return Matrix(result)

    def __mul__(self, other):
        self.__check_dims(other, "mul")

        result = np.zeros_like(self.values)
        for i in range(self.shape[0]):
            for j in range(self.shape[1]):
                result[i, j] = self.values[i, j] * other.values[i, j]

        return Matrix(result)

    def __matmul__(self, other):
        self.__check_dims(other, "matmul")

        result = np.zeros((self.shape[0], other.shape[1]))

        for i in range(self.shape[0]):
            for j in range(other.shape[1]):
                for k in range(self.shape[1]):
                    result[i, j] += self.values[i, k] * other.values[k, j]
        
        return Matrix(result)

    def __str__(self):
        return str(self.values)