import numbers
import numpy as np


class StrMixin:
    def __init__(self, values):
        self.values = values

    def __str__(self):
        return str(self.values)

    def __repr__(self):
        return f'type: {type(self)} \n values: \n {str(self.values)}'

class AttrMixin:
    def __init__(self, values):
        self.values = values

    @property
    def values(self):
        return self.__values

    @values.setter
    def values(self, value):
        self.__values = value

class WriterMixin:
    def write(self, file_name):
        with open(file_name, 'w') as f:
            f.write(str(self))

class ArithmeticMixin(np.lib.mixins.NDArrayOperatorsMixin):
    __whitelisted_types = (np.ndarray, numbers.Number)
    
    def __init__(self, values):
        self.values = values

    def __array_ufunc__(self, ufunc, method, *inputs, **kwargs):
        for x in inputs:
            if not isinstance(x, (ArithmeticMixin, *self.__whitelisted_types)):
                return NotImplemented()
                
        inputs = (x.values for x in inputs)

        return type(self)(getattr(ufunc, method)(*inputs, **kwargs))


class Matrix(AttrMixin, WriterMixin, StrMixin, ArithmeticMixin):
    pass