import numpy as np
from matrix_mixin import Matrix

def main():
    np.random.seed(0)
    arr1 = np.random.randint(0, 10, (10, 10))
    arr2 = np.random.randint(0, 10, (10, 10))

    mat1, mat2 = Matrix(arr1), Matrix(arr2)

    (mat1 + mat2).write("artifacts/medium/matrix+.txt")
    (mat1 * mat2).write("artifacts/medium/matrix*.txt")
    (mat1 @ mat2).write("artifacts/medium/matrix@.txt")

if __name__ == "__main__":
    main()