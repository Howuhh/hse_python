import os
import shutil
from typing import List
from pdflatex import PDFLaTeX
from howuhh_hw1 import ast_viz


def add_graphics_package(document, path):
    return "\\usepackage{graphicx}\n\graphicspath{ {./" + path + "/} }\n" + document

def add_graphics(document, path):
    return "\\includegraphics[width = 15cm]{" + path + "}\n" + document

def generate_document_class(class_name):
    def generator(document):
        return f"\documentclass\u007b{class_name}\u007d \n" + document
    return generator

def generate_env(env_name):
    def generator(document):
        return f"\\begin\u007b{env_name}\u007d\n" + document + f"\n\end\u007b{env_name}\u007d"
    return generator

# will work better with decorators
article_document = generate_document_class("article")
tabular_env = generate_env("tabular")
document_env = generate_env("document")
center_env = generate_env("center")

def make_header(row):
    cols = " c |" * len(row)    
    return "{ |" + cols + "}" + "\n\hline\n"

def make_row(row):
    table_row = " & ".join(str(s) for s in row)
    return f"{table_row} \\\ \n\hline \n"

def generate_table(table: List[List[str]]) -> str:
    header = make_header(table[0])
    rows = " ".join(map(make_row, table))

    table = article_document(
        document_env(
           center_env(
                tabular_env(header + rows)
            )
        )
    )
    return table

def generate_table_and_img(table):
    ast_viz.vis_ast(ast_viz.fib, "artifacts/ast.png")
    
    header = make_header(table[0])
    rows = " ".join(map(make_row, table))
    # почти lisp
    table = article_document(
        add_graphics_package(
            document_env(
                add_graphics(
                    center_env(
                        tabular_env(header + rows)
                        ), path="ast")
                ), path="artifacts")
        )
        
    return table

if __name__ == '__main__':
    # task 1
    table = [
        ["Column 1", "Column 2", "Column 3"],
        ["row\_name\_1", 10, 12],
        ["row\_name\_2", 9, 13],
        ["row\_name\_3", 8, 14]
    ]

    with open("artifacts/one.tex", "w") as out:
        out.write(generate_table(table))

    # task 2
    with open("artifacts/two.tex", "w") as out:
        out.write(generate_table_and_img(table))

    pdfl = PDFLaTeX.from_texfile("artifacts/two.tex")
    pdf, log, completed_process = pdfl.create_pdf(keep_pdf_file=True, keep_log_file=False)
    os.remove('artifacts/two.tex')
    os.remove('artifacts/ast.png')
    shutil.move('two.pdf', "artifacts/two.pdf")       
    

