import ast
import inspect
import networkx as nx

from typing import Callable

import matplotlib.pyplot as plt

def fib(n):
    nums = [0, 1]
    for i in range(1, n - 1):
        nums.append(nums[i] + nums[i - 1])

    return nums


class NetworkVisitor(ast.NodeVisitor):
    def __init__(self):
        self.stack = []
        self.graph = nx.DiGraph()

    def __get_label(self, node):
        label = str(type(node)).split(".")[-1][:-2]

        if isinstance(node, ast.FunctionDef):
            label = label + ": " + node.name

        # дальше здесь можно обрабатывать все методы, но это запарно

        return label

    def generic_visit(self, node):
        if self.stack:
            parent_name = self.stack[-1]
        else:
            parent_name = None

        self.stack.append(str(node))
        self.graph.add_node(str(node), label=self.__get_label(node))

        if parent_name is not None:
            self.graph.add_edge(parent_name, str(node))

        super(self.__class__, self).generic_visit(node)
        self.stack.pop()

    def clear_tree(self):
        black_list = ("Load", "Module")

        candidates = []
        for node in list(self.graph.nodes):
            for r in black_list:
                if r in node:
                    candidates.append(node)

        return candidates


def vis_ast(func: Callable, save_path: str):
    ast_struct = ast.parse(inspect.getsource(func)).body[0]
    
    ast_walker = NetworkVisitor()
    ast_walker.visit(ast_struct)
    ast_walker.graph.remove_nodes_from(ast_walker.clear_tree())
    
    G = ast_walker.graph
    p = nx.drawing.nx_pydot.to_pydot(G)
    p.write_png(save_path)


if __name__ == "__main__":
    vis_ast(fib, "artifacts/ast.png")
