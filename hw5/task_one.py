import os
import uuid
import aiohttp
import asyncio
import argparse

LINK = 'https://picsum.photos/400/600'


async def download_images(num_images, save_path):
    # single task
    async def __download_image(session, image_url, save_path):
        async with session.get(image_url) as response:
            with open(os.path.join(save_path, str(uuid.uuid4()) + ".jpeg"), "wb") as f:
                f.write(await response.read())
    
    # downloading images
    async with aiohttp.ClientSession() as session:
        tasks = [
            asyncio.create_task(__download_image(session, LINK, save_path)) for _ in range(num_images)
        ]
        return await asyncio.gather(*tasks)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--num', type=int, default=1, help="Number of files to download")
    parser.add_argument('--save_path', default="artifacts", help="Path for saving files")
    args = parser.parse_args()

    os.makedirs(args.save_path, exist_ok=True)

    asyncio.run(download_images(args.num, args.save_path))


if __name__ == "__main__":
    main()