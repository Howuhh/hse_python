from time import perf_counter
from contextlib import contextmanager

from threading import Thread
from multiprocessing import Process

# https://stackoverflow.com/questions/33987060/python-context-manager-that-measures-time
@contextmanager
def time():
    start = perf_counter()
    yield lambda: perf_counter() - start


def fib(n):
    nums = [0, 1]
    for i in range(1, n - 1):
        nums.append(nums[i] + nums[i - 1])

    return nums


def main():
    FIB_NUM = int(50_000)

    with time() as seq_time:
        [fib(FIB_NUM) for _ in range(10)]
    
    with time() as thread_time:
        jobs = [Thread(target=fib, args=(FIB_NUM, )) for _ in range(10)]
        [j.start() for j in jobs]
        [j.join() for j in jobs]

    with time() as process_time:
        jobs = [Process(target=fib, args=(FIB_NUM, )) for _ in range(10)]
        [j.start() for j in jobs]
        [j.join() for j in jobs]

    with open("artifacts/easy.txt", "w") as f:
        f.write(f"Sequential time: {seq_time()}\nThread time: {thread_time()}\nProcess time: {process_time()}")


if __name__ == "__main__":
    main()