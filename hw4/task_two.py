import os
import math
import logging
import concurrent.futures

from task_one import time
from collections import defaultdict


def __integrate_step(args):
    f, x, step, logger = args 
    if logger is not None:
        logger.info(f'Call with args x={round(x, 4)}, step={round(step, 4)}')
    
    return f(x) * step


def integrate_processes(f, a, b, *, n_jobs=1, n_iter=1000, logger=None):
    step = (b - a) / n_iter
    args = [(f, a + i * step, step, logger) for i in range(n_iter)]
    with concurrent.futures.ProcessPoolExecutor(max_workers=n_jobs) as executor:
        res = list(executor.map(__integrate_step, args))

    return sum(res)


def integrate_threads(f, a, b, *, n_jobs=1, n_iter=1000, logger=None):
    step = (b - a) / n_iter
    args = [(f, a + i * step, step, logger) for i in range(n_iter)]
    with concurrent.futures.ThreadPoolExecutor(max_workers=n_jobs) as executor:
        res = list(executor.map(__integrate_step, args))

    return sum(res)


def main():
    NUM_ITER = 1000
    N_JOBS = range(1, 2 * os.cpu_count() + 1)

    results = defaultdict(list)
    for name, f in (("processes", integrate_processes), ("threads", integrate_threads)):
        logging.basicConfig(
            filename=os.path.join("artifacts/medium", f"log_{name}.txt"), 
            level=logging.INFO, 
            format="%(asctime)s;%(levelname)s;%(message)s"
        )
        logger = logging.getLogger(os.path.basename(__file__))

        for n_jobs in N_JOBS:
            with time() as t:
                f(math.cos, 0, math.pi / 2, n_jobs=n_jobs, n_iter=NUM_ITER, logger=logger)
                results[name].append(t())

    with open("artifacts/medium/times.txt", "w") as f:
        f.write("n_jobs processes threads\n")
        for n_jobs, p_time, t_time in zip(N_JOBS, results["processes"], results["threads"]):
            f.write(f"{n_jobs} {p_time} {t_time}\n")

if __name__ == "__main__":
    main()